package com.zaykwet.util;

import android.support.annotation.NonNull;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import com.zaykwet.emarket.model.CarDetail;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Utils
 */
public class Util {

	public Util() {

	}

	public String loadResourceFile(String filename) throws IOException {
		try {
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename);
			return inputStreamToString(is);
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@NonNull
	public static String inputStreamToString(InputStream is) throws IOException {
		String json;
		int size = is.available();
		byte[] buffer = new byte[size];
		is.read(buffer);
		is.close();
		json = new String(buffer, "UTF-8");
		return json;
	}

	static public List<CarDetail> loadCars(String fileName) {
		Util util = new Util();
		String json = null;
		try {
			json = util.loadResourceFile(fileName);
			return parseCarDetails(json);
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error(e);
		}
	}

	public static List<CarDetail> parseCarDetails(String json) throws IOException {
		Moshi moshi = new Moshi.Builder().build();
		Type listMyData = Types.newParameterizedType(List.class, CarDetail.class);
		JsonAdapter<List<CarDetail>> adapter = moshi.adapter(listMyData);
		return adapter.fromJson(json);
	}
}

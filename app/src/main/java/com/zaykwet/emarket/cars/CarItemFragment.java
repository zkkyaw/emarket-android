package com.zaykwet.emarket.cars;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zaykwet.data.CarsRepository;
import com.zaykwet.data.InMemoryCarsRepository;
import com.zaykwet.emarket.R;
import com.zaykwet.emarket.model.CarDetail;
import com.zaykwet.util.Util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import rx.subscriptions.CompositeSubscription;

/**
 * A fragment representing a list of Items.
 */
public class CarItemFragment extends Fragment {

	private CarItemActionListener mListener;
	private CarItemAdapter adapter;
	private CompositeSubscription mSubscriptions;
	private CarsRepository repository;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public CarItemFragment() {
	}

	@SuppressWarnings("unused")
	public static CarItemFragment newInstance() {
		CarItemFragment fragment = new CarItemFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSubscriptions = new CompositeSubscription();
		try {
			InputStream is = getActivity().getAssets().open("cars-1.json");
			List<CarDetail> cars = Util.parseCarDetails(Util.inputStreamToString(is));
			repository = new InMemoryCarsRepository(cars);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_caritem_list, container, false);

		// Set the adapter
		if (view instanceof RecyclerView) {
			setupRecyclerView((RecyclerView) view);
		}
		return view;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof Activity) {
			if (context instanceof CarItemActionListener) {
				mListener = (CarItemActionListener) context;
			} else {
				throw new RuntimeException(context.toString()
						+ " must implement CarItemActionListener");
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		onRefresh();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mSubscriptions.unsubscribe();
	}

	private void setupRecyclerView(RecyclerView view) {
		Context context = view.getContext();
		view.setLayoutManager(new LinearLayoutManager(context));
		adapter = new CarItemAdapter(mListener);
		view.setAdapter(adapter);
	}

	public void onRefresh() {
		mSubscriptions.unsubscribe();
		repository.list().subscribe(adapter);
	}

}

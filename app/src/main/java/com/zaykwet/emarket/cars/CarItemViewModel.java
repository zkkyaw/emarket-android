package com.zaykwet.emarket.cars;

import android.databinding.BaseObservable;

import com.zaykwet.emarket.model.CarDetail;

/**
 * Created by mbikyaw on 24/3/16.
 */
public class CarItemViewModel extends BaseObservable {
    public CarDetail car;
    final private CarItemActionListener listener;
    public CarItemViewModel(CarItemActionListener listener, CarDetail car) {
        this.car = car;
        this.listener = listener;
    }
}

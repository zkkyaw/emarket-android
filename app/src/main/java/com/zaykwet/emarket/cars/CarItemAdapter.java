package com.zaykwet.emarket.cars;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zaykwet.emarket.R;
import com.zaykwet.emarket.databinding.CarItemBinding;
import com.zaykwet.emarket.model.CarDetail;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

/**
 *
 */
public class CarItemAdapter extends RecyclerView.Adapter<CarItemAdapter.ViewHolder> implements Action1<List<CarDetail>> {

	private List<CarDetail> mValues;
	private final CarItemActionListener mListener;

	public CarItemAdapter(CarItemActionListener listener) {
		mValues = new ArrayList<CarDetail>();
		mListener = listener;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		CarItemBinding view = DataBindingUtil.inflate(
				LayoutInflater.from(parent.getContext()),
				R.layout.car_item,
				parent,
				false);

		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		CarDetail car = mValues.get(position);
		holder.binding.setViewModel(new CarItemViewModel(mListener, car));
	}

	@Override
	public int getItemCount() {
		return mValues.size();
	}

	@Override
	public void call(List<CarDetail> carDetails) {
		mValues = carDetails;
		notifyDataSetChanged();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public CarItemBinding binding;

		public ViewHolder(CarItemBinding view) {
			super(view.cardView);
			this.binding = view;
		}

	}
}

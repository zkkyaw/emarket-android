package com.zaykwet.emarket.car;

import android.content.Context;

import com.zaykwet.emarket.model.CarDetail;

/**
 * Created by mbikyaw on 23/3/16.
 */
public class CarDetailViewModel {
    private Context context;
    private CarDetail carDetail;

    public CarDetailViewModel(Context context, CarDetail carDetail) {
        this.context = context;
        this.carDetail = carDetail;
    }
}

package com.zaykwet.emarket.model;

/**
 * Car detail data.
 */
public class CarDetail {
    public String item_description;
    public String item_name;
}

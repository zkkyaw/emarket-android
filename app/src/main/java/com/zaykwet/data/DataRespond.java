package com.zaykwet.data;

/**
 * A typical paging respond from Laravel server.
 */
public class DataRespond<T> {
    public T data;
}

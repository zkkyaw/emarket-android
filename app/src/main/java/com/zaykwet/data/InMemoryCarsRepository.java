package com.zaykwet.data;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import com.zaykwet.emarket.model.CarDetail;
import com.zaykwet.util.Util;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import rx.Observable;

/**
 * Concrete implementation to load notes from the a data source for testing.
 */
public class InMemoryCarsRepository implements CarsRepository {
	private List<CarDetail> cars;

	public InMemoryCarsRepository(List<CarDetail> cars) {
		this.cars = cars;
	}

	@Override
	public Observable<List<CarDetail>> list() {

		return Observable.just(cars);
	}
}

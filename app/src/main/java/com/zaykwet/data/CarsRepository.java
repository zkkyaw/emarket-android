package com.zaykwet.data;

import java.util.List;

import rx.Observable;
import com.zaykwet.emarket.model.CarDetail;

/**
 * Main entry point for accessing cars data.
 */
public interface CarsRepository {
	Observable<List<CarDetail>> list();
}

package com.zaykwet.data;

import com.zaykwet.emarket.model.CarDetail;
import com.zaykwet.util.Util;

import org.junit.Test;

import java.util.List;

import rx.observers.TestSubscriber;

import static junit.framework.Assert.assertEquals;

/**
 * Created by kyawtun on 25/3/16.
 */
public class InMemoryCarsRepositoryTest {

	@Test
	public void testList() throws Exception {
		List<CarDetail> list = new Util().loadCars("cars-1.json");
		InMemoryCarsRepository repository = new InMemoryCarsRepository(list);
		TestSubscriber<List<CarDetail>> testSubscriber = new TestSubscriber<>();
		repository.list().subscribe(testSubscriber);
		testSubscriber.assertNoErrors();
		List<CarDetail> cars = testSubscriber.getOnNextEvents().get(0);
		assertEquals(10, cars.size());
		assertEquals("Toyota  Verossa  2001", cars.get(0).item_name);
	}
}